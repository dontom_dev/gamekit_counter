// get countdown element
var countdownCircle = document.getElementsByClassName('circle');
var countdownNumberEl = document.getElementsByClassName('countdown-number');

// time for countdown in minutes
var min = 0.2;

// countdown in seconds
var countdown = min * 60;

// set first count down
var countdownShow = displayTime(countdown);
countdownNumberEl[0].textContent = countdownShow;
countdownNumberEl[1].textContent = countdownShow;

// set circle animation time
var minString = countdown + "s";
countdownCircle[0].style.animationDuration = minString;
countdownCircle[1].style.animationDuration = minString;


// **** Functions ****
// function responsible for calculating and displaying time inside circle
myTimer = setInterval(function() {
    countdown -= 1;
    countdownShow = displayTime(countdown);
    countdownNumberEl[0].textContent = countdownShow;
    countdownNumberEl[1].textContent = countdownShow;

    // stopping counter
    if(countdown == 0) {
        clearInterval(myTimer);
        countdownNumberEl[0].textContent = "";
        countdownNumberEl[1].textContent = "";
        min = 0;
        minString = countdown + "s";
        countdownCircle[0].style.animationDuration = minString;
        countdownCircle[1].style.animationDuration = minString;
    }
}, 1000);

// function to calculate time that left and transform it into string
function displayTime(time) {
    return Math.floor(time / 60) + " min" + "\n" + time % 60 + " sek";
}